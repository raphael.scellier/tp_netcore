﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Saisissez votre nom : ");
            string nom = (Console.ReadLine());
            Console.WriteLine("Saisissez votre prenom : ");
            string prenom = (Console.ReadLine());
            Console.WriteLine("Quel est votre age ?");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine(String.Format("Salutations {0} {1}!",prenom,nom));
            int naissance = DateTime.Now.Year-age;
            Console.WriteLine(string.Format("Vous etes nes en {0}",naissance));
            if(((naissance % 4 == 0) && (naissance % 100 != 0)) || (naissance % 400 == 0)){
                Console.WriteLine("Vous etes nes une annee bissextile.");
            }
            else{
                Console.WriteLine("Vous etes nes une annee non bissextile.");
            }
            Console.WriteLine("Jusqu'a combien dois-je compter ?");
            int nbTours = int.Parse(Console.ReadLine());
            for(int i=1;i<=nbTours;i++){
                Console.Write(string.Format("{0} ",i));
            }
            Console.WriteLine();
        }
    }
}
